import { Container, Grid, TableContainer, Table, Paper, TableHead, TableBody, TableRow, TableCell, Pagination  } from "@mui/material";
import { useEffect, useState } from "react";

function DataTable() {
    const numRow = 3;//so dong tren 1 trang
    const [rows, setRows] = useState([]);
    const [totalPage, setTotalPage] = useState(0);
    const [curPage, setCurPage] = useState(1);

    const fetchAPI = async (url) => {
        const response = await fetch(url);

        const data = response.json();

        return data;        
    }

    const handleChangePage = (event, value) => {
        setCurPage(value);
    }
    useEffect(() => {
        fetchAPI("https://jsonplaceholder.typicode.com/users")
        .then((data) => {
            setTotalPage(Math.ceil(data.length/numRow));
            setRows(data.slice((curPage-1) * numRow, curPage * numRow));
        })
        .catch((error) => {
            console.error(error.message);
        })
    }, [curPage])

    return (
        <Container>
            <Grid container>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell align="right">Name</TableCell>
                                <TableCell align="right">Username</TableCell>
                                <TableCell align="right">Email</TableCell>
                                <TableCell align="right">Phone</TableCell>
                                <TableCell align="right">Website</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {rows.map((row) => (
                                <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.username}</TableCell>
                                <TableCell align="right">{row.email}</TableCell>
                                <TableCell align="right">{row.phone}</TableCell>
                                <TableCell align="right">{row.website}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container justifyContent={"end"} mt={3}>
                <Grid item>
                    <Pagination count={totalPage} defaultPage={curPage} onChange={handleChangePage} />
                </Grid>                    
            </Grid>
        </Container>
    );
}

export default DataTable;